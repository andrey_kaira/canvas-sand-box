import 'dart:async';
import 'dart:math';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeContent extends StatefulWidget {
  @override
  _HomeContentState createState() => _HomeContentState();
}



void main() => runApp(MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Test'),
        ),
        body: HomeContent(),
      ),
    ));

class _HomeContentState extends State<HomeContent>
    with TickerProviderStateMixin {
  double percentage = 0.0;
  double newPercentage = 0.0;
  AnimationController  animationController;

  Timer _timer;
  final timeout = const Duration(seconds: 28);
  startTimeout() {
    _timer = new Timer.periodic(timeout, (Timer timer) {
      animationController.forward(from: 0.0);
    });
  }

  @override
  void initState() {
    super.initState();

    setState(() {

      startTimeout();
      percentage = 0.0;
    });
    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: 30), upperBound: pi * 2,
    );
  }

  @override
  Widget build(BuildContext context) {
    animationController.forward(from: 0.0);
    return new Center(
      child: new Container(
        height: 200.0,
        width: 200.0,
        child: RotationTransition(
          turns: Tween(begin: 0.0, end: 1.0).animate(animationController),
          child: new CustomPaint(
            foregroundPainter: new MyPainter(),
          ),
        ),
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  MyPainter();

  @override
  void paint(Canvas canvas, Size size) {
    Paint centerBrush = new Paint()
      ..color = Colors.white
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 20;
    Paint greenBrush = new Paint()
      ..color = Colors.green
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 50;
    Paint blueBrush = new Paint()
      ..color = Colors.blue
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 50;
    Paint yellowBrush = new Paint()
      ..color = Colors.yellow
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 50;
    Paint purpleBrush = new Paint()
      ..color = Colors.purple
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 50;
    Paint redBrush = new Paint()
      ..color = Colors.red
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 50;
    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = min(size.width / 2, size.height / 2);
    canvas.drawCircle(center, radius, centerBrush);
    double arcAngle;
    arcAngle = 2 * pi * (1);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, purpleBrush);
    arcAngle = 2 * pi * (0.9);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, yellowBrush);
    arcAngle = 2 * pi * (0.8);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, blueBrush);
    arcAngle = 2 * pi * (0.7);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, greenBrush);
    arcAngle = 2 * pi * (0.6);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, redBrush);
    arcAngle = 2 * pi * (0.5);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, purpleBrush);
    arcAngle = 2 * pi * (0.4);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, yellowBrush);
    arcAngle = 2 * pi * (0.3);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, blueBrush);
    arcAngle = 2 * pi * (0.2);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, greenBrush);
    arcAngle = 2 * pi * (0.1);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, redBrush);
    arcAngle = 2 * pi * (0.01);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 2,
        arcAngle, false, purpleBrush);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
